#!/bin/bash

# Check for argument
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path>"
    echo "Example: $0 /dev4/Tue/"
    exit 1
fi

# Get the argument
PATH_SEGMENT="$1"

# Start the timer
SECONDS=0

# Base URL and target directory adjusted with the provided path segment
BASE_URL="https://lcgpackages.web.cern.ch/tarFiles/nightlies${PATH_SEGMENT}"
TARGET_DIR_PACKAGES="/cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${PATH_SEGMENT}"
TARGET_DIR_VIEWS="/cvmfs/sft-nightlies-test.cern.ch/lcg/views/${PATH_SEGMENT}"

# Start the transaction
sudo -i -u cvsft-nightlies-test<<EOF

cvmfs_server transaction sft-nightlies-test.cern.ch/lcg/nightlies/${PATH_SEGMENT}

# Fetch the list of .txt files from the web page
txt_files=\$(curl -s "${BASE_URL}" | grep -oE 'href="[^"]+\.txt"' | cut -d'"' -f2)

# Temporary folder for storing downloaded .txt files
TMP_DIR=\$(mktemp -d)

# Download all .txt files to the temporary directory
for file in \$txt_files; do
    curl -s "${BASE_URL}\${file}" -o "\${TMP_DIR}/\${file}"
done

# Process each downloaded .txt file
for file in \$(ls \$TMP_DIR); do
    awk -F', ' '{
        for(i=1;i<=NF;i++) {
            split(\$i, arr, ": ");
            if(arr[1] == "NAME") name = arr[2];
            else if(arr[1] == "VERSION") version = arr[2];
            else if(arr[1] == "PLATFORM") platform = arr[2];
        }
        print name "/" version "/" platform "/"; # this line remains unchanged
        print name "/"; # this line is added to produce just the packagename directory path
    }' "\${TMP_DIR}/\${file}" | sort -u | while read dir; do
        full_dir="${TARGET_DIR_PACKAGES}\${dir}"
        mkdir -p "\$full_dir"
        full_dir="${TARGET_DIR_VIEWS}\${dir}"
        mkdir -p "\$full_dir"
        if [[ \$dir == *"/"*"/"* ]]; then
            # only touch the .cvmfscatalog file for Packagename directory
            touch "${TARGET_DIR_PACKAGES}\$(echo "\$dir" | cut -d'/' -f1)/.cvmfscatalog"
        fi
    done
    echo "file \$file finished at \$SECONDS seconds"
done

# Clean up the temporary directory
rm -rf "\$TMP_DIR"

# Publish the changes
cvmfs_server publish sft-nightlies-test.cern.ch

# Print the duration
echo "Script took \$SECONDS seconds to complete."
EOF