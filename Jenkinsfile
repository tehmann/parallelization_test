// Jenkinsfile for nightlies
// change extraction path to /cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/_STACK_/_DAY_/_PLATFORM_/_NAME_/_VERSION_


pipeline {
    agent none

    parameters {
        string(name: 'DAY', defaultValue: 'Tue', description: 'Day for nightlies')
    }

    stages {
        // test stage to activate lxcvmfs171, 172 and 173 and ensure all are online
        stage('Ensure Agents Online') {
            matrix {
                axes {
                    axis {
                        name 'NODE_NAME'
                        values 'lxcvmfs171', 'lxcvmfs172', 'lxcvmfs173'
                    }
                }
                stages {
                    stage('Check Node') {
                        agent {
                            label "${NODE_NAME}"
                        }
                        steps {
                            script {
                                echo "Checking node ${NODE_NAME}"
                                sh 'echo "Node is online"'
                            }
                        }
                    }
                }
            }
        }
        stage('Matrix Build and Test') {
            matrix {
                axes {
                    axis {
                        name 'STACK'
                        values 'dev3', 'dev4'
                    }
                    axis {
                        name 'PLATFORM'
                        values 'x86_64-centos7-gcc11-dbg', 'x86_64-centos7-gcc11-opt', 'x86_64-el9-gcc11-opt'
                        //'x86_64-centos7-gcc12-dbg', 'x86_64-centos7-gcc12-opt', 'x86_64-el9-clang16-dbg', 'x86_64-el9-clang16-opt', 'x86_64-el9-gcc11-opt', 'x86_64-el9-gcc12-dbg', 'x86_64-el9-gcc12-opt', 'x86_64-el9-gcc13-dbg', 'x86_64-el9-gcc13-opt', 'x86_64-ubuntu2004-gcc9-opt', 'x86_64-ubuntu2204-gcc11-opt'
                    }
                }

                stages {
                    stage('Install Packages') {
                        agent {
                            label 'cvmfs-nightlies-publisher'
                        }
                        steps {
                            script  {
                                //def weekday = (new Date()).format("EEE")
                                def weekday = params.DAY
                                def dir1 = "/cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${weekday}/${PLATFORM}"
                                def dir2 = "/cvmfs/sft-nightlies-test.cern.ch/lcg/views/${STACK}/${weekday}/${PLATFORM}"
                                def dir3 = "/cvmfs/sft-nightlies-test.cern.ch/lcg/views/${STACK}/latest"
                                def missingDirs = []

                                // Check which directories don't exist
                                if (!fileExists("${dir1}")) {
                                    missingDirs.add(dir1)
                                }
                                if (!fileExists("${dir2}")) {
                                    missingDirs.add(dir2)
                                }
                                if (!fileExists("${dir3}")) {
                                    missingDirs.add(dir3)
                                }
                                echo "Missing directories: ${missingDirs}"

                                // If any of the directories don't exist, create them
                                if (missingDirs.size() > 0) {
                                    echo "waiting for lock since ${sh(script: 'date +%s%3N', returnStdout: true).trim()}"
                                    lock(label: 'cvmfs-nightlies-publisher', quantity: 0) {
                                        echo "lock aquired at ${sh(script: 'date +%s%3N', returnStdout: true).trim()}"
                                        sleep 10
                                        def commands = """
                                            cvmfs_server transaction
                                            ${missingDirs.collect { "mkdir -p ${it} && touch ${it}/.cvmfscatalog" }.join('\n')}
                                            cvmfs_server publish
                                        """
                                        sh label: 'CVMFS Transaction', script: "sudo -u cvsft-nightlies-test bash -c '''${commands}'''"
                                        echo "lock released at ${sh(script: 'date +%s%3N', returnStdout: true).trim()}"
                                    }
                                    sleep 10
                                }

                                lock(label: "cvmfs-nightlies-publisher", quantity: 1) {
                                    echo "Running install script at ${sh(script: 'date +%s%3N', returnStdout: true).trim()}"
                                    def success = sh(script: """
                                        chmod +x lcgcmake_files/cvmfs_install.sh
                                        ./lcgcmake_files/cvmfs_install.sh '${STACK}' '${weekday}' '${PLATFORM}' 'nightly' '${env.WORKSPACE}' 'true'
                                    """, returnStatus: true)
                                    echo "Success: ${success}"

                                    if (success == 0){
                                        lock('latest_view_sim_link') {
                                        sh label: 'cvmfs_install', script: """
                                            export PLATFORM=${PLATFORM}
                                            chmod +x lcgcmake/jenkins/view_latest_simlinks.sh
                                            lcgcmake/jenkins/view_latest_simlinks.sh '${STACK}' '${weekday}' '${PLATFORM}'
                                        """
                                        }
                                    }
                                    echo "Finished install script at ${sh(script: 'date +%s%3N', returnStdout: true).trim()}"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
