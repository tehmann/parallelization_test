#!/bin/bash
BASE_URL="$1"
STACK="$2"
DAY="$3"

sudo -i -u cvsft-nightlies-test<<EOF
cvmfs_server transaction sft-nightlies-test.cern.ch
# Process each file to create required folders and .cvmfscatalog files
txt_files=\$(curl -s "${BASE_URL}" | grep -oE 'href="[^"]+\.txt"' | cut -d'"' -f2)
for file in \$txt_files; do
    PLATFORM=\$(echo \$file | sed -n 's/^LCG_'"$STACK"'_\(.*\)\.txt$/\1/p')
    if [ ! -z "\$PLATFORM" ]; then
        mkdir -p "/cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/\$PLATFORM" || echo "Failed to create folder /cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/\$PLATFORM"
        mkdir -p "/cvmfs/sft-nightlies-test.cern.ch/lcg/views/${STACK}/${DAY}/\$PLATFORM" || echo "Failed to create folder /cvmfs/sft-nightlies-test.cern.ch/lcg/views/${STACK}/${DAY}/\$PLATFORM"
        touch "/cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/\$PLATFORM/.cvmfscatalog" || echo "Failed to create .cvmfscatalog file in /cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/\$PLATFORM"
    fi
done
cvmfs_server publish sft-nightlies-test.cern.ch
EOF