#!/bin/bash -x
# This script updatest the simlinks in simlinks at /cvmfs/sft-nightlies-test.cern.ch/lcg/views/${LCG_VERSION}/latest

# Arguments for debugging, usually form environment variables
LCG_VERSION="$1"
weekday="$2"
PLATFORM="$3"


REPOSITORY=sft-nightlies-test.cern.ch
cvmfs_user=cvsft-nightlies-test

sudo -i -u ${cvmfs_user}<<EOF
cvmfs_server transaction -t 6000 sft-nightlies-test.cern.ch/lcg/views/${LCG_VERSION}/latest

rm -f /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/latest/${PLATFORM}
cd /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/latest
ln -s ../${weekday}/${PLATFORM}
cd /home

cvmfs_server publish sft-nightlies-test.cern.ch/lcg/views/${LCG_VERSION}/latest
EOF