pipeline {
    agent none
    parameters {
        string(name: 'STACK',    defaultValue: 'dev4',                  description: 'Stack for nightlies')
        string(name: 'DAY',      defaultValue: 'Tue',                   description: 'Day for nightlies')
        string(name: 'PLATFORM', defaultValue: 'aarch64-el9-gcc13-opt', description: 'Platform for nightlies')
    }
    stages {
        stage('Checkout and Stash') {
            agent {
                label 'cvmfs-nightlies-publisher'
            }
            steps {
                checkout scm
                stash includes: 'create_all_needed_folders.sh', name: 'createFoldersScript'
            }
        }
        stage('Working on nightlies'){
            options{
                lock(resource: 'nightliesLock-${params.STACK}-${params.DAY}')
            }
            stages {
                stage ('Create Folder Structure'){
                    agent {
                        label 'cvmfs-nightlies-publisher'
                    }
                    steps {
                        unstash 'createFoldersScript'
                        sh """
                            # Make sure the script is executable
                            chmod +x create_all_needed_folders.sh
                            
                            # Execute the script with the required argument
                            ./create_all_needed_folders.sh /dev4/Tue/
                        """
                    }
                }
                stage('Parallel Execution') {
                    agent {
                        label 'startup-jobs'
                    }
                    steps {
                        //node ('cvmfs-nightlies-publisher') {
                            script {
                                def processedPackages = 0  // This variable will store the number of packages processed
                                def numNodes = 3  // You can adjust this to your needs
                                def parallelTasks = [:] // This map will store the parallel tasks
                                def nodeProcessedPackages = [:]  // This map will store packages count for each node
                                def nodeStartTime = [:]  // This map will store start time for each node
                                def allPackages = readFile('Package_blueprints/LCG_dev4_aarch64-el9-gcc13-opt.txt').split("\n").findAll { !it.startsWith('#') } // Remove comment lines from the list
                    
                                def getPackage = {
                                    def package_line = ''
                                    lock('PackageVarLock') {
                                        if (allPackages.size() > 0) {
                                            package_line = allPackages[0]
                                            allPackages.remove(0)
                                        }
                                    }
                                    return package_line
                                }

                                for (int i = 1; i <= numNodes; i++) {
                                    def outerNodeName = "Machine ${i} Operations"
                                    parallelTasks[outerNodeName] = {
                                        node ('cvmfs-nightlies-publisher') { 
                                            def pkgCount = 0
                                            def pkg
                                            def nodeName = "${NODE_NAME}"
                                            // file name is nodeName/LCG_Stack_Platform.txt
                                            def fileName = "${nodeName}/LCG_${params.STACK}_${params.PLATFORM}.txt"
                                            while ((pkg = getPackage()) != '') {
                                                // 0. Start timer for this package
                                                def startTime = System.currentTimeMillis()

                                                // 1. Create a temporary txt file for the package at ${nodeName}/LCG_dev4_aarch64-el9-gcc13-opt.txt
                                                writeFile(file: fileName, text: pkg)

                                                // 2. Extract NAME, VERSION, and PLATFORM
                                                def details = pkg.split(', ').collectEntries {
                                                    def (key, value) = it.split(": ").toList()
                                                    [(key): value]
                                                }
                                                def name = details['NAME']
                                                def version = details['VERSION']
                                                def platform = details['PLATFORM']

                                                def elapsed_preinstall = (System.currentTimeMillis() - startTime)
                                                // 3. Start transaction, lease path and install package
sh """
sudo -i -u cvsft-nightlies-test bash<<-EOF
echo "Test: \$(date +%s%3N)"

declare START_TIME_1
declare START_TIME_2
declare START_TIME_3
declare END_TIME

echo "${name},\$(date +%s%3N)" >> /home/cvsft-nightlies-test/comparison/command_times.txt

cvmfs_server transaction sft-nightlies-test.cern.ch/lcg/nightlies/${params.STACK}/${params.DAY}/${name}/${version}/${platform}/
echo "${name},\$(date +%s%3N)" >> /home/cvsft-nightlies-test/comparison/command_times.txt

python3 /home/cvsft-nightlies-test/lcgcmake/jenkins/lcginstall.py -d ${fileName} -p /cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${params.STACK}/${params.DAY}/ -u file://${env.WORKSPACE} -a https://lcgpackages.web.cern.ch/tarFiles/sources -r ${params.STACK} -y
echo "${name},\$(date +%s%3N)" >> /home/cvsft-nightlies-test/comparison/command_times.txt

cvmfs_server publish sft-nightlies-test.cern.ch/lcg/nightlies/${params.STACK}/${params.DAY}/${name}/${version}/${platform}/
echo "${name},\$(date +%s%3N)" >> /home/cvsft-nightlies-test/comparison/command_times.txt

EOF
"""

                                                def elapsed_install = (System.currentTimeMillis() - startTime)

                                                lock('ProcessedPackagesLock') {
                                                    processedPackages++
                                                    echo "Processed packages so far: $processedPackages"
                                                }
                                                pkgCount++  // Increase the count for this node

                                                def elapsed_total = (System.currentTimeMillis() - startTime)
                                                echo "Time taken to install ${name}: ${elapsed_total} milliseconds"
                                                lock('WriteTimeLock') {
                                                    sh "sudo -u cvsft-nightlies-test sh -c \"echo '${name},${elapsed_preinstall} milliseconds,${nodeName},preinstall' >> /home/cvsft-nightlies-test/comparison/package_install_times.txt\""
                                                    sh "sudo -u cvsft-nightlies-test sh -c \"echo '${name},${elapsed_install} milliseconds,${nodeName},install' >> /home/cvsft-nightlies-test/comparison/package_install_times.txt\""
                                                    sh "sudo -u cvsft-nightlies-test sh -c \"echo '${name},${elapsed_total} milliseconds,${nodeName},finished' >> /home/cvsft-nightlies-test/comparison/package_install_times.txt\""
                                                }
                                            }
                                            nodeProcessedPackages[nodeName] = pkgCount
                                        }
                                    }
                                }

                                parallel(parallelTasks)

                                // Print the number of packages processed by each node
                                nodeProcessedPackages.each { key, value ->
                                    echo "${key} processed ${value} packages."
                                }
                            }
                        //}
                    }
                }
                stage('Create View') {
                    steps {
                        node('cvmfs-nightlies-publisher') {
sh """
sudo -i -u cvsft-nightlies-test bash<<-EOF
cvmfs_server transaction sft-nightlies-test.cern.ch
python3 /home/cvsft-nightlies-test/lcgcmake/cmake/scripts/create_lcg_view.py --delete -p ${params.PLATFORM} -l /cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${params.STACK}/${params.DAY} /cvmfs/sft-nightlies-test.cern.ch/lcg/views
cvmfs_server publish sft-nightlies-test.cern.ch
EOF
"""
                        }
                    }
                }
            }
        }
    }
}

