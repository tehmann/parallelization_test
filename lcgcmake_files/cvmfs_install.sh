#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

# VARIABLES ADDED BY ME, SHOULD BE REMOVED EXCEPT weekday
LCG_VERSION="$1"
weekday="$2"
PLATFORM="$3"
BUILDMODE="$4"
WORKSPACE="$5"
VIEWS_CREATION="$6"

REPOSITORY='sft-nightlies-test.cern.ch'
exit_code=0

#weekday=`date +%a`


if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_user=cvsft-nightlies-test
else
  cvmfs_user=cvsft
fi
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch
if [[ "${BUILDMODE}" == "nightly" ]]; then
  # -t 6000, retry for 6000 seconds
  cvmfs_server transaction -t 6000 ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
else
  cvmfs_server transaction -t 6000 sft.cern.ch
fi
retVal=\$?
if [ "\$retVal" == "17" ]; then
   # return value 17 is transaction in progress (EEXIST)
   # After 100 minutes in nightly mode the opened transaction is forcibly aborted
   # Only during the night (between 7pm and 8am)
   H=`date +%H`
   if [[ "${BUILDMODE}" == "nightly" && (19 -le "\$H" || "\$H" -lt 8) ]]; then
       echo "Forcing transaction abortion"
       cvmfs_server abort -f ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
       cvmfs_server transaction ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
   else
       exit 1
   fi
elif [ "\$retVal" != "0" ]; then
   echo "Error starting transaction"
   exit 1
fi

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo Weekday:   $weekday
echo BuildMode: $BUILDMODE
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
export weekday=${weekday}

abort=0

if [[ "${BUILDMODE}" == "nightly" ]]; then
    export NIGHTLY_MODE=1
    $WORKSPACE/lcgcmake/jenkins/clean_nightlies.py ${LCG_VERSION} ${PLATFORM} ${weekday} cvmfs
// ADDED PLATFORM TO PATH
    rm -f /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-$PLATFORM
    rm -f /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-unstable-$PLATFORM
    rm -f /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/LCG_externals_$PLATFORM.txt
    rm -f cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/LCG_generators_$PLATFORM.txt

    echo "$WORKSPACE/lcgcmake/jenkins/lcginstall.py -y -u https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/${weekday} -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_${PLATFORM}.txt -p /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM} -e cvmfs"
    $WORKSPACE/lcgcmake/jenkins/lcginstall.py -y -u https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/${weekday} -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_${PLATFORM}.txt -p /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM} -e cvmfs
    if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
    else
      echo "there is an error installing the packages. Let's give it a chance though ..."
    fi


    cd  /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}

    wget https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/$weekday/isDone-$PLATFORM
    wget https://lcgpackages.web.cern.ch/tarFiles/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-$PLATFORM

    ls -l | grep isDone

    $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM} ${PLATFORM} ${LCG_VERSION} RELEASE

// GOING BACK FOR TRANSACTION, CANT PUBLISH FROM WITHIN THE FOLDER
    cd -



    if [ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-$PLATFORM" ] && [ xtrue = "x${VIEWS_CREATION}" ]; then
      cvmfs_server publish ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}

      echo "PUBLISHING DONE, VIEW IS NEXT"
      echo "The installation of the nightly is completed with all packages, let's go for the view creation"
    

// START TRANSACTION FOR VIEW CREATION

      cvmfs_server transaction -t 6000 ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}

// ADDED PLATFORM FOLDER TO PATH, BEFORE IT STOPPED AT NIGHTLIES, BUT WORKS LIKE THAT, NOT SURE WHATS BETTER
      $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py --loglevel ERROR -l /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM} -r ${LCG_VERSION} -p ${PLATFORM} -d -B /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM} -n
      
      if [ "\$?" == "0" ]; then
        cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
        echo "View created, updating latest symlink"
      else
        // in the old script we do nothing, but due the more granular transaction handling we could abort here?
        cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
        echo "There was an error creating view, not updating latest symlink"
        exit_code=1
      fi
    elif [ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-unstable-$PLATFORM" ]; then
        cvmfs_server publish ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
        echo "The installation has not been completed and we do not create the view"
    else
        cvmfs_server publish ${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}
        echo "SOME OTHER ERRORS, LETS SEE WHAT IFS DIDNT WORK"
        if [ ! -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-$PLATFORM" ]; then
            echo "path /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-$PLATFORM does not exist"
        fi
        if [ ! -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-unstable-$PLATFORM" ]; then
            echo "path /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/${weekday}/${PLATFORM}/isDone-unstable-$PLATFORM does not exist"
        fi
        if [ xtrue != "x${VIEWS_CREATION}" ]; then
            echo "xtrue != x${VIEWS_CREATION}"
        fi
    fi

elif [[ "${BUILDMODE}" == "release" ]]; then
// TRANSACTION IS STARTED ON SFT.CERN.CH AT THE TOP OF THE SCRIPT, NOT PARALLELISABLE
    if [[ "${UPDATELINKS}" == "false" ]]; then
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -u https://lcgpackages.web.cern.ch/tarFiles/releases -a https://lcgpackages.web.cern.ch/tarFiles/layered_releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
    else
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -u https://lcgpackages.web.cern.ch/tarFiles/releases -a https://lcgpackages.web.cern.ch/tarFiles/layered_releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs --update
    fi

    if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
      abort=1
    else
      echo "there is an error installing the packages. Exiting ..."
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi

    cd  /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION}
    $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py . $PLATFORM ${LCG_VERSION} RELEASE
    export BUILDMODE=${BUILDMODE}
    if [ ${VIEWS_CREATION} == "true" ]; then
      test "\$abort" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases -p $PLATFORM -r LCG_${LCG_VERSION} -d -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM
    fi

    if [ "\$?" == "0"  -o ${VIEWS_CREATION} == "false" ]; then
      echo "The creation of the views has worked"
      cd $HOME
      cvmfs_server publish sft.cern.ch
    else
      echo "The creation of the views has not worked. We exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi

elif [[ "${BUILDMODE}" == "limited" ]]; then
    if [[ "${UPDATELINKS}" == "false" ]]; then
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -o -u https://lcgpackages.web.cern.ch/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
    else
        $WORKSPACE/lcgcmake/jenkins/lcginstall.py -o -u https://lcgpackages.web.cern.ch/tarFiles/releases -r ${LCG_VERSION} -d LCG_${LCG_VERSION}_$PLATFORM.txt --update -p /cvmfs/sft.cern.ch/lcg/releases -e cvmfs
    fi

    if [ "\$?" == "0" ]; then
      echo "Installation script has worked, we go on"
      abort=1
    else
      echo "there is an error installing the packages. Exiting ..."
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi
    if [ ${VIEWS_CREATION} == "true" ]; then
      test "\$abort" == "1" && $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION} -p $PLATFORM -d -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM
    fi

    if [ "\$?" == "0" -o ${VIEWS_CREATION} == "false" ]; then
      echo "The creation of the views has worked"
      cd $HOME
      cvmfs_server publish sft.cern.ch
    else
      echo "The creation of the views has not worked. We exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi
fi
EOF

# return exit code from the view_creation script.
# This might has to be changed in the future, if we want to have a more granular error handling
exit $exit_code
