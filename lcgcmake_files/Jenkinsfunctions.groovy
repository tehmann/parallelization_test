//---Dynamic Global Variables------------------------------------------------------------------------------------------- 
def PLATFORM = 'UNKNOWN'
def barePLATFORM = 'UNKNOWN'
def urlPLATFORM = 'UNKNOWN'
def CTEST_TAG = ''
def BUILDHOSTNAME = ''
def MAC12_INSTALL_HOSTNAME = ''
def CVMFS_REVISION = '0'

//----------------------------------------------------------------------------------------------------------------------
//---Functions for Steps------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

def initScript() {
  def BTYPE = [Release: 'opt', Debug: 'dbg']
  currentBuild.displayName = "#${BUILD_NUMBER}" + ' ' + params.LCG_VERSION + '-' + params.LABEL + '-' + params.COMPILER + '-' + BTYPE[params.BUILDTYPE]
  return 'done'
}

def checkFolders() {
  // This function checks if the folders for the current stack/day/platform exist in the cvmfs repository
  // If they don't exist, it creates them
  def weekday = (new Date()).format("EEE")
  def platform = sh(returnStdout: true, script: 'lcgcmake/jenkins/getPlatform.py').trim()
  def dir1 = "/cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${params.LCG_VERSION}/${weekday}/${platform}"
  def dir2 = "/cvmfs/sft-nightlies-test.cern.ch/lcg/views/${params.LCG_VERSION}/${weekday}/${platform}"
  def dir3 = "/cvmfs/sft-nightlies-test.cern.ch/lcg/views/${params.LCG_VERSION}/latest"
  def missingDirs = []

  // Check which directories don't exist
  if (!fileExists("${dir1}")) {
      missingDirs.add(dir1)
  }
  if (!fileExists("${dir2}")) {
      missingDirs.add(dir2)
  }
  if (!fileExists("${dir3}")) {
      missingDirs.add(dir3)
  }
  echo "Missing directories: ${missingDirs}"

  // If any of the directories don't exist, create them
  if (missingDirs.size() > 0) {
      lock(label: 'cvmfs-nightlies-publisher', quantity: 0) {
          sleep 10
          def commands = """
              cvmfs_server transaction
              ${missingDirs.collect { "mkdir -p ${it} && touch ${it}/.cvmfscatalog" }.join('\n')}
              cvmfs_server publish
          """
          sh label: 'CVMFS Transaction', script: "sudo -u cvsft-nightlies-test bash -c '''${commands}'''"
      }
      sleep 10
  }
}

def buildPackages() {
  catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE') {
    sh label: 'build_and_test', script: """
      source lcgcmake/jenkins/jk-setup.sh $BUILDTYPE $COMPILER
      env | sort | sed 's/:/:?     /g' | tr '?' '\n'
      ctest -VV -DCTEST_LABELS=Release  -S lcgcmake/jenkins/lcgcmake-build.cmake
    """
  }
  //---Fail the release if the build stage fails-------------------------------------------------------------------------
  if ( params.BUILDMODE == 'release' && currentBuild.result == 'UNSTABLE') {
    echo 'Setting the build state to FAILURE for BUILDMODE equal to release'
    currentBuild.result = 'FAILURE'
  }
  //---Update a number of variables after the build step-----------------------------------------------------------------
  PLATFORM = sh(returnStdout: true, script: 'lcgcmake/jenkins/getPlatform.py').trim()
  barePLATFORM = ([PLATFORM.split('-')[0].split('\\+')[0]]+PLATFORM.split('-')[1..3]).join('-').replace('gcc7','gcc8').replace('dbg','opt')
  urlPLATFORM = PLATFORM.replaceAll('\\+','%2B')
  CTEST_TAG = sh(returnStdout: true, script: 'cat build/Testing/TAG').trim()
  BUILDHOSTNAME = sh(returnStdout: true, script: '[ ${container} ] &&  echo ${NODE_NAME}-${container} ||  hostname').trim()
  CVMFS_REVISION = '0'
}

def copyToEOS() {
  sh label: 'copy_to_eos', script: """
    export PLATFORM=${PLATFORM}
    if [ -d /cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/xrootd/4.12.3/${barePLATFORM} ]; then
      set +x; source /cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/xrootd/4.12.3/${barePLATFORM}/xrootd-env.sh; set -x
    fi
    lcgcmake/jenkins/copytoEOS.sh
    if [[ $BUILDMODE == nightly ]]; then
      lcgcmake/jenkins/copytoLatest.sh
    fi
  """
}

def cvmfsVolume() { 
  return params.BUILDMODE == 'nightly' ? 'sft-nightlies.cern.ch' : 'sft.cern.ch'
}

def testLABEL(String label, String version) {
  if (version =~ 'cuda') {
    return 'cuda10' 
  }
  else if( label =~ 'mac') {
    return label + '-cvmfs'
  }
  else {
    return label
  }
}

def preinstallMacOS() {
  if (params.BUILDMODE == 'nightly') {
    def today = (new Date()).format("EEE")
    def yesterday = (new Date() - 1).format("EEE")
    def volume = '/Users/Shared/cvmfs/sft-nightlies.cern.ch'
    def prefix = volume + '/lcg/nightlies'
    def views = volume + '/lcg/views'
    sh  label:  'preinstall-macos', script: """
      sudo umount ${volume} || true
      rm -rf ${prefix}/${LCG_VERSION}/${yesterday}
      rm -rf ${prefix}/${LCG_VERSION}/${today}
      mkdir -p ${prefix}/${LCG_VERSION}/${today}
      mkdir -p ${views}/${LCG_VERSION}
      mkdir -p preinstall
      rm -rf preinstall/*
      cd preinstall
      ../lcgcmake/bin/lcgcmake reset
      ../lcgcmake/bin/lcgcmake config --version ${LCG_VERSION} \
                                      --prefix ${prefix} \
                                      --nightly
      ../lcgcmake/bin/lcgcmake install ${TARGET}
    """
  }
  else if (params.BUILDMODE == 'release') {
    def volume = '/Users/Shared/cvmfs/sft.cern.ch'
    def prefix = volume + '/lcg/releases'
    def views = volume + '/lcg/views'
    sh  label:  'preinstall-macos', script: """
      unset RELEASE_MODE
      sudo umount ${volume} || true
      mkdir -p ${prefix}/LCG_${LCG_VERSION}
      rm -rf ${prefix}/LCG_${LCG_VERSION}/*
      mkdir -p ${views}/LCG_${LCG_VERSION}
      mkdir -p preinstall
      rm -rf preinstall/*
      cd preinstall
      ../lcgcmake/bin/lcgcmake reset
      ../lcgcmake/bin/lcgcmake config --version LCG_${LCG_VERSION} \
                                      --prefix ${prefix} \
                                      -o LCG_ADDITIONAL_REPOS='' \
                                      --toolchain ../lcgcmake/cmake/toolchain/heptools-${LCG_VERSION}.cmake
      ../lcgcmake/bin/lcgcmake install ${TARGET}
    """
  }
  MAC12_INSTALL_HOSTNAME = sh(returnStdout: true, script: 'echo ${NODE_NAME}').trim()
  BUILDHOSTNAME = sh(returnStdout: true, script: 'hostname').trim()
}


def preinstallMacOSOnLinux() {
  if (params.BUILDMODE == 'nightly') {
    def today = (new Date()).format("EEE")
    def yesterday = (new Date() - 1).format("EEE")
    def volume = '/Users/Shared/cvmfs/sft-nightlies.cern.ch'
    def prefix = volume + '/lcg/nightlies'
    def views = volume + '/lcg/views'
    sh  label:  'preinstall-macos', script: """
      LCG_HOST_OSVERS=`echo ${params.LABEL} | grep -o "[0-9]*"`
      export PRETEND_MAC=1

      # we need otool and install_name_tool as they are called on macos in the PATH
      source /cvmfs/sft.cern.ch/lcg/contrib/clang/14/x86_64-centos7/setup.sh
      ln -sf /cvmfs/sft.cern.ch/lcg/contrib/clang/14/x86_64-centos7/bin/llvm-otool otool
      ln -sf /cvmfs/sft.cern.ch/lcg/contrib/clang/14/x86_64-centos7/bin/llvm-install-name-tool install_name_tool
      export PATH=`pwd`:\$PATH

      mkdir -p ${prefix}/${LCG_VERSION}/${today}
      mkdir -p ${views}/${LCG_VERSION}
      rm -rf preinstall
      mkdir -p preinstall
      cd preinstall
      ../lcgcmake/bin/lcgcmake reset
      ../lcgcmake/bin/lcgcmake config --version ${LCG_VERSION} \
                                      --prefix ${prefix} \
                                      --nightly \
                          --target_platform ${PLATFORM} \
                          -o APPLE=ON LCG_HOST_SYSTEM=${PLATFORM} LCG_HOST_OS=mac LCG_HOST_OSVERS=\${LCG_HOST_OSVERS}

      ../lcgcmake/bin/lcgcmake install ${TARGET}
    """
  }
  else if (params.BUILDMODE == 'release') {
    def volume = '/Users/Shared/cvmfs/sft.cern.ch'
    def prefix = volume + '/lcg/releases'
    def views = volume + '/lcg/views'
    sh  label:  'preinstall-macos', script: """
      LCG_HOST_OSVERS=`echo ${params.LABEL} | grep -o "[0-9]*"`
      export PRETEND_MAC=1

      # we need otool and install_name_tool as they are called on macos in the PATH
      source /cvmfs/sft.cern.ch/lcg/contrib/clang/14/x86_64-centos7/setup.sh
      ln -sf /cvmfs/sft.cern.ch/lcg/contrib/clang/14/x86_64-centos7/bin/llvm-otool otool
      ln -sf /cvmfs/sft.cern.ch/lcg/contrib/clang/14/x86_64-centos7/bin/llvm-install-name-tool install_name_tool
      export PATH=`pwd`:\$PATH

      unset RELEASE_MODE
      mkdir -p ${prefix}/LCG_${LCG_VERSION}
      rm -rf ${prefix}/LCG_${LCG_VERSION}/*
      mkdir -p ${views}/LCG_${LCG_VERSION}
      mkdir -p preinstall
      rm -rf preinstall/*
      cd preinstall
      ../lcgcmake/bin/lcgcmake reset
      ../lcgcmake/bin/lcgcmake config --version LCG_${LCG_VERSION} \
                                      --prefix ${prefix} \
                                      -o LCG_ADDITIONAL_REPOS='' \
                                      --toolchain ../lcgcmake/cmake/toolchain/heptools-${LCG_VERSION}.cmake \
                          --target_platform ${PLATFORM} \
                          -o APPLE=ON LCG_HOST_SYSTEM=${PLATFORM} LCG_HOST_OS=mac LCG_HOST_OSVERS=\${LCG_HOST_OSVERS}
      ../lcgcmake/bin/lcgcmake install ${TARGET}
    """
    }
    MAC12_INSTALL_HOSTNAME = sh(returnStdout: true, script: 'echo ${NODE_NAME}').trim()

}


def installCVMFS() {
  def installationSteps = {
    if (params.LABEL == 'mac12arm') {
      sh label: 'cvmfs_install', script: """
        export PLATFORM=${PLATFORM}
        export BUILDHOSTNAME=${MAC12_INSTALL_HOSTNAME}
        lcgcmake/jenkins/cvmfs_install_mac.sh
      """
    } else if (params.LABEL =~ 'mac') {
      sh label: 'cvmfs_install', script: """
        export PLATFORM=${PLATFORM}
        export BUILDHOSTNAME=${BUILDHOSTNAME}
        lcgcmake/jenkins/cvmfs_install_mac.sh
      """
    } else {
      def success = sh label: 'cvmfs_install', script: """
        export PLATFORM=${PLATFORM}
        lcgcmake/jenkins/cvmfs_install.sh
      """
      if (success == 0){
        lock('latest_view_sim_link') {
          sh label: 'cvmfs_install', script: """
            export PLATFORM=${PLATFORM}
            chmod +x lcgcmake/jenkins/view_latest_simlinks.sh
            lcgcmake/jenkins/view_latest_simlinks.sh
          """
        }
      }
    }
    //---Update variable after the install step---------------------------------------------------------------------------
    def volume = [release: 'sft.cern.ch', nightly: 'sft-nightlies.cern.ch']
    CVMFS_REVISION = sh(returnStdout: true, script: "attr -qg revision /var/spool/cvmfs/${cvmfsVolume()}/rdonly/")
  }
   // lock everything when buildmode is release or limited because its not parallelisable
  if (params.BUILDMODE == 'release' || params.BUILDMODE == 'limited') {
    lock('cvmfs_install') {
      installationSteps()
    }
  }
  else {
    lock(label: "cvmfs-nightlies-publisher", quantity: 1) {
      installationSteps()
    }
  }
}

def waitForCVMFS() {
  sh  label:  'wait-for-cvmfs', script: """
    lcgcmake/jenkins/wait_for_cvmfs.sh ${cvmfsVolume()} ${CVMFS_REVISION}
  """
}

def cloneRepository(String repo, String project = 'sft/lcgtests', String branch = 'master') {
  dir(repo) {
    git branch: branch,
        credentialsId: '7c3a9bea-6657-46fc-901d-bef5c7e0061c',
        url: "https://gitlab.cern.ch/${project}/${repo}.git"
  }
}

def testWithViews() {
  sh  label: 'test-with-views', script: """
    export PLATFORM=${PLATFORM}
    export BUILDHOSTNAME=${BUILDHOSTNAME}
    rm -rf test_build
    mkdir -p test_build/Testing
    echo "${CTEST_TAG}" > test_build/Testing/TAG
    export BUILD_PATH=/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/$LCG_VERSION/`date +%a`
    source /cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM/setup.sh
    if [[ $PLATFORM == *slc6* ]]; then export KERAS_BACKEND=theano; fi
    if [[ $PLATFORM == *aarch64* ]]; then export OPENBLAS_NUM_THREADS=50; fi
    ctest -DPLATFORM=$PLATFORM -DBUILD_PATH=\${BUILD_PATH} -V -DCTEST_LABELS=".*" -S lcgcmake/jenkins/lcgtest-test.cmake
  """
}

def testShell() {
  sh  label: 'test-shell',  script: """
    export PLATFORM=${PLATFORM}
    export BUILDHOSTNAME=${BUILDHOSTNAME}
    if [ \$(uname) != 'Darwin' ]; then
      export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.17.1/Linux-`uname -m`/bin:\${PATH}
    fi
    rm -rf csh_build
    mkdir -p csh_build/Testing
    echo "${CTEST_TAG}" > csh_build/Testing/TAG
    export VIEW=/cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM
    ctest -V -DCTEST_LABELS=".*" -S lcgcmake/jenkins/lcgtest-csh.cmake
  """
}

def testPythonImport() {
  sh  label: 'test-pythonimport', script: """
    set +e
    export PLATFORM=${PLATFORM}
    export BUILDHOSTNAME=${BUILDHOSTNAME}
    export MODE=`echo "${CTEST_TAG}" |  sed '2q;d'`
    export CTEST_TIMESTAMP=`echo "${CTEST_TAG}" | head -1`
    cd lcg-import-test
    python3 python_test.py ${LCG_VERSION} ${PLATFORM}
    exit_code=\$?
    testxml=\$WORKSPACE/Test.xml
    checksum=\$(md5sum \$testxml | cut -d ' ' -f 1)
    curl -s --upload-file \$testxml "http://cdash.cern.ch/submit.php?project=LCGSoft&FileName=${BUILDHOSTNAME}___${LCG_VERSION}-${urlPLATFORM}___\${CTEST_TIMESTAMP}-\${MODE}___XML___Test.xml&build=${LCG_VERSION}-${urlPLATFORM}&site=${BUILDHOSTNAME}&stamp=\${CTEST_TIMESTAMP}-\${MODE}&MD5=\${checksum}"
    exit \$exit_code
  """
}

def createRPMS() {
  sh  label: 'create-rpms', script: """
    cd ${WORKSPACE}/install
    export RPM_REVISION_NUMBER=${RPM_REVISION_NUMBER}
    ${WORKSPACE}/lcgcmake/jenkins/package_release.py ${WORKSPACE} ${LCG_VERSION} ${PLATFORM} ${TARGET} ${BUILDMODE} ${COMPILER}
  """
}

def publishRPMS() {
  sh  label: 'publish-rpms', script: """
    export EOS_MGM_URL=root://eosproject-l.cern.ch/
    export LABEL=`echo ${PLATFORM} | cut -f2 -d-`
    export ARCH=`echo ${PLATFORM} | cut -f1 -d-`
    export BTYP=`echo ${PLATFORM} | cut -f4 -d-`
    export DEBUG=""
    if [ "\${BTYP}" == "dbg" ]; then
       export DEBUG="/debug"
    fi
    if [[ $LCG_VERSION =~ ^([0-9]+).* ]]; then
       export NUM=`echo ${LCG_VERSION} | sed -E 's/([0-9]+)(.*)/\\1/g'`
    else
       echo "No RPM_REPOSITORY provided and cannot deduce from version $LCG_VERSION"
       exit 1
    fi
    REPOROOT=${RPM_REPOSITORY}
    if [ "$RPM_REPOSITORY" == "" ]; then
       REPOROOT=/eos/project/l/lcg/www/lcgpackages/lcg/repo/\${LABEL: -1}/
       REPO=/eos/project/l/lcg/www/lcgpackages/lcg/repo/\${LABEL: -1}/\${ARCH}\${DEBUG}/LCG_\${NUM}
       REPOPACK=/eos/project/l/lcg/www/lcgpackages/lcg/repo/\${LABEL: -1}/\${ARCH}\${DEBUG}/Packages
    else
       REPO=${RPM_REPOSITORY}/\${ARCH}\${DEBUG}/LCG_\${NUM}
       REPOPACK=${RPM_REPOSITORY}/\${ARCH}\${DEBUG}/Packages
    fi
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
    eosfusebind
    if [ ${RPM_DRYRUN} == 'yes' ]; then
       lcgcmake/jenkins/complete_rpms_actions.py ${LCG_VERSION} \$REPOROOT \$(echo ${PLATFORM} | sed -e 's/-/_/g') --dryrun
    else
       lcgcmake/jenkins/complete_rpms_actions.py ${LCG_VERSION} \$REPOROOT \$(echo ${PLATFORM} | sed -e 's/-/_/g')
       echo "Creating/updating packages RPMs repository on \$REPOPACK ..."
       createrepo --update --workers=20 \$REPOPACK 
       echo "Creating/updating meta RPMs repository on \$REPO ..."
       createrepo --update --workers=20 \$REPO
    fi
  """
}

def testRPMS() {
  sh  label: 'test-rpms', script: """
    PLATFORM=$PLATFORM
    LCG_RELEASE=$LCG_VERSION
    LCG_RELEASE_BASE=\$(echo \$LCG_RELEASE | cut -d '_' -f 1 | sed -e 's/python3//g' | sed -e 's/[^0-9]//g')
    export YUM0="\${LABEL: -1}"
    PLATFORMU=\$(echo \$PLATFORM | sed -e 's/-/_/g')
    TARGETRPM=LCG_\${LCG_RELEASE}_\${PLATFORMU}
    export YUM1=""
    if [[ "$RPM_REPOSITORY" == *"/dev/"* ]]; then
       export YUM1="dev"
    fi
    export YUM2=""
    if [[ "$PLATFORM" == *"-dbg"* ]]; then
       export YUM2="-debug"
    fi
    curl https://lcgpackages.web.cern.ch/lcg/etc/yum.repos.d/lcg\${YUM0}\${YUM1}\${YUM2}.repo > /etc/yum.repos.d/lcg.repo
    cat /etc/yum.repos.d/lcg.repo
    # rpm --rebuilddb
    # yum update -y
    yum install -y \${TARGETRPM}
  """
}

def publishINFO() {
  sh  label: 'publish-info', script: """
    export TYPE=$BUILDMODE
    export LCG_RELEASE=$LCG_VERSION
    export DESCRIPTION='Publication of release ${LCG_VERSION}'
    export WORKSPACE=$WORKSPACE/lcginfo
    lcginfo/db_publish.sh
  """
}

return this;
