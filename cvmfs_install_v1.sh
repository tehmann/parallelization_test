#!/bin/bash -x
STACK="$1"
DAY="$2"
PLATFORM="$3"

sudo -i -u cvsft-nightlies-test bash<<-EOF
cvmfs_server transaction sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/${PLATFORM}
python3 /home/cvsft-nightlies-test/lcgcmake/jenkins/lcginstall.py -d LCG_${STACK}_${PLATFORM}.txt -p /cvmfs/sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/${PLATFORM} -u "https://lcgpackages.web.cern.ch/tarFiles/nightlies/${STACK}/${DAY}" -a https://lcgpackages.web.cern.ch/tarFiles/sources -r ${STACK} -y
cvmfs_server publish sft-nightlies-test.cern.ch/lcg/nightlies/${STACK}/${DAY}/${PLATFORM}
EOF